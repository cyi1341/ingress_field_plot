import matplotlib.pyplot as plt
import itertools
from collections import Counter

def plot_lines(lines, title, color='b-', legend=None):
    plt.figure()
    for line in lines:
        x = [line[0][0], line[1][0]]
        y = [line[0][1], line[1][1]]
        plt.plot(x, y, color, label=legend)
    plt.title(title)
    plt.xlabel('X')
    plt.ylabel('Y')
    if legend:
        plt.legend()

def get_all_lines(points):
    return list(itertools.combinations(points, 2))

def get_intersecting_lines(points):
    lines = get_all_lines(points)
    intersecting_lines = []
    for i in range(len(lines)):
        for j in range(i+1, len(lines)):
            if is_intersecting(lines[i], lines[j]):
                intersecting_lines.append((lines[i], lines[j]))
    return intersecting_lines

def get_non_intersecting_lines(points):
    all_lines = get_all_lines(points)
    intersecting_lines = get_intersecting_lines(points)
    return [line for line in all_lines if line not in intersecting_lines]

def plot_all_possible_lines(points):
    lines = get_all_lines(points)
    plot_lines(lines, 'All Possible Lines')

def plot_intersecting_lines(points):
    lines = get_intersecting_lines(points)
    for i, (line1, line2) in enumerate(lines):
        plt.figure()
        x1 = [line1[0][0], line1[1][0]]
        y1 = [line1[0][1], line1[1][1]]
        plt.plot(x1, y1, 'b-', label='Line 1')
        x2 = [line2[0][0], line2[1][0]]
        y2 = [line2[0][1], line2[1][1]]
        plt.plot(x2, y2, 'r-', label='Line 2')
        plt.title(f'Intersection {i+1}')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend()


def plot_non_intersecting_lines(points):
    lines = get_non_intersecting_lines(points)
    plot_lines(lines, 'Non-intersecting Lines', 'g-')

def plot_non_intersecting_combinations(points):
    intersecting_lines = get_intersecting_lines(points)
    intersecting_lines = [tuple(map(tuple, line)) for pair in intersecting_lines for line in pair]
    all_lines = get_all_lines(points)
    all_lines = [tuple(map(tuple, line)) for line in all_lines]
    pivot_lines = []
    temp_deletion = []
    temp_non_deletion = []

    while True:
        intersecting_count = Counter(itertools.chain(*intersecting_lines))
        pivot_line = intersecting_count.most_common(1)[0][0]
        pivot_lines.append(pivot_line)

        temp_intersecting_lines = [line for line in intersecting_lines if pivot_line not in line]
        for idx, line in enumerate(intersecting_lines):
            if pivot_line in line:
                temp_deletion.append({line: [idx]})

                if idx % 2 == 0:  
                    opp_idx = idx + 1
                else:
                    opp_idx = idx - 1

                opp_line = intersecting_lines[opp_idx]
                
                temp_non_deletion.append({opp_line: [opp_idx]})
        print("Intersecting lines: ", intersecting_lines)

        while any(pivot_line in line for line in temp_intersecting_lines):
            pivot_line = next(line for line in temp_intersecting_lines if pivot_line in line)
            temp_intersecting_lines = [line for line in temp_intersecting_lines if pivot_line not in line]
            for idx, line in enumerate(intersecting_lines):
                if pivot_line in line:
                    temp_deletion.append({line: [idx]})

                    if idx % 2 == 0:  
                        opp_idx = idx + 1
                    else:
                        opp_idx = idx - 1

                    opp_line = intersecting_lines[opp_idx]
                    
                    temp_non_deletion.append({opp_line: [opp_idx]})
        temp_deletion_keys = [line for temp_del in temp_deletion for line in temp_del.keys()]
        final_lines = [line for line in all_lines if line not in temp_deletion_keys]
        print("Temp deletion: ", temp_deletion)
        print("Temp non-deletion: ", temp_non_deletion)

        new_intersecting_lines = []
        for i in range(len(final_lines)):
            for j in range(i+1, len(final_lines)):
                line1 = final_lines[i]
                line2 = final_lines[j]
                if is_intersecting(line1, line2):
                    new_intersecting_lines.append((line1, line2))

        if not new_intersecting_lines:
            break
        else:
            intersecting_lines = new_intersecting_lines

    final_lines = [list(map(list, line)) for line in final_lines]
    plot_lines(final_lines, 'Non-intersecting Combinations', 'g-')

def is_intersecting(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return False

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div

    if (min(line1[0][0], line1[1][0]) < x < max(line1[0][0], line1[1][0])) and \
       (min(line1[0][1], line1[1][1]) < y < max(line1[0][1], line1[1][1])) and \
       (min(line2[0][0], line2[1][0]) < x < max(line2[0][0], line2[1][0])) and \
       (min(line2[0][1], line2[1][1]) < y < max(line2[0][1], line2[1][1])):
        return True

    return False

# Example points
points = [[1, 2], [1, 4], [5, 3], [2, 3], [3, 7]]

# Plot the graphs
plot_all_possible_lines(points)
plot_intersecting_lines(points)
plot_non_intersecting_lines(points)
plot_non_intersecting_combinations(points)

# Show all the plots
plt.show()

